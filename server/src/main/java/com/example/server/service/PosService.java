package com.example.server.service;

import com.example.server.model.Pos;
import com.example.server.repository.PosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PosService {

    @Autowired
    private PosRepository posRepository;

    public List<Pos> view () {
        return posRepository.findAll();
    }

    public void save(Pos pos) {
        posRepository.save(pos);
    }

    public Pos viewById(Long id) {
        return posRepository.findById(id).get();
    }

    public void update(Pos pos) {
        posRepository.save(pos);
    }

    public void delete(Long id) {
        posRepository.findById(id);
        posRepository.deleteById(id);
    }
}
