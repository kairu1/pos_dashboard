package com.example.server;


import org.json.simple.JSONObject;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@SpringBootApplication
public class ServerApplication {

	@Bean
	public JSONObject jsonObject() {
		return new JSONObject();
	}

	@Bean
	public WebMvcConfigurer webMvcConfigurer() {
		return new WebMvcConfigurerAdapter() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/users/login").allowedOrigins("http://localhost:4200");
				registry.addMapping("/users/register").allowedOrigins("http://localhost:4200");
				registry.addMapping("/list-pos").allowedOrigins("http://localhost:4200");
				registry.addMapping("/list-pos/{id}").allowedOrigins("http://localhost:4200/");
			}
		};
	}

	public static void main(String[] args) {
		SpringApplication.run(ServerApplication.class, args);
	}

}
