package com.example.server.repository;

import com.example.server.model.Pos;
import org.springframework.data.jpa.repository.JpaRepository;


public interface PosRepository extends JpaRepository<Pos, Long> {


}
