package com.example.server.model;


import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Entity
@Setter @Getter
@NoArgsConstructor
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull(message = "Username cannot be empty")
    private String username;

    @NotNull(message = "Email field cannot be empty")
    @Email(message = "Enter a valid email, please")
    private String email;

    private @NonNull String password;

    public User(@NotNull(message = "Username cannot be empty") String username,
                @NotNull(message = "Email field cannot be empty") @Email(message = "Enter a valid email, please") String email,
                @NonNull String password) {
        this.username = username;
        this.email = email;
        this.password = password;
    }
}
