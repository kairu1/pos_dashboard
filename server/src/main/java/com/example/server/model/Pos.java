package com.example.server.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@Entity
@Setter
@Getter
public class Pos {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @NotNull(message = "Serial Number cannot be empty")
    @Size(max=8, min=4)
    private String serialNumber;

    @NotNull(message = "Make cannot be empty")
    @Size(max=30)
    private String make;

    @NotNull(message = "Owner cannot be empty")
    @Size(max=30)
    private String owner;

    @NotNull(message = "Status cannot be empty")
    private String status;

    public Pos(@NotNull(message = "Serial Number cannot be empty") @Size(max = 8, min = 4) String serialNumber, @NotNull(message = "Make cannot be empty") @Size(max = 30) String make, @NotNull(message = "Owner cannot be empty") @Size(max = 30) String owner, @NotNull(message = "Status cannot be empty") String status) {
        this.serialNumber = serialNumber;
        this.make = make;
        this.owner = owner;
        this.status = status;
    }

    public Pos() {
    }
}
