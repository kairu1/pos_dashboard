package com.example.server.controller;

import com.example.server.model.Pos;
import com.example.server.service.PosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class PosController {


    @Autowired
    private PosService posService;

    @GetMapping("/list-pos")
    public ResponseEntity<Object> view () {
        Map<String, Object> response = new HashMap<>();
        List<Pos> list =  posService.view();
        response.put("Pos", list);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


    @RequestMapping(value = "/list-pos", method = RequestMethod.POST)
    public ResponseEntity<Object> addPos(@RequestBody Pos pos) {
        Map<String, Object> response = new HashMap<>();
        posService.save(pos);
        response.put("message", "Pos Added Successfully.");
        return new  ResponseEntity<>(response, HttpStatus.CREATED);
    }
    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(value = "/list-pos/{id}" ,  method = RequestMethod.DELETE)
    public  ResponseEntity<Object> delete(@PathVariable("id")  Long id) {
        Map<String, String> response = new HashMap<>();
        posService.delete(id);
        response.put("message","Deleted   successfully");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @CrossOrigin(origins = "http://localhost:4200/{id}")
    @RequestMapping(value = "/list-pos/{id}", method = RequestMethod.PUT)
    public  ResponseEntity<Object> updatePos(@PathVariable("id") long id, @RequestBody Pos pos) {
        Map<Long, Pos> response = new HashMap<>();
        pos.setId(id);
        posService.update(pos);
        response.put(id, pos);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
    @GetMapping("/list-pos/{id}")
    public ResponseEntity<Object> view (@PathVariable  Long id) {
        Map<String, Object> response = new HashMap<>();
        Pos pos =  posService.viewById(id);
        response.put("Pos", pos);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

}
