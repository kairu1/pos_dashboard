import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import { DataTablesModule } from 'angular-datatables';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { posReducer } from './pos-state/pos.reducer';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { HomeComponent } from './components/home/home.component';
import { PosListComponent } from './components/pos-list/pos-list.component';
import { PosComponent } from './components/pos/pos.component';

import { PosService } from './services/pos-service/pos.service';
import { ProfileComponent } from './components/profile/profile.component';
import { PosSalesComponent } from './components/pos-sales/pos-sales.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    PosListComponent,
    PosComponent,
    ProfileComponent,
    PosSalesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    DataTablesModule,
    StoreModule.forRoot({pos: posReducer})
  ],
  providers: [PosService],
  bootstrap: [AppComponent]
})
export class AppModule { }
