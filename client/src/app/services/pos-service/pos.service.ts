import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Pos } from '../../models/pos-model/pos';

@Injectable({
  providedIn: 'root'
})
export class PosService {

  private posUrl: string;

  constructor(private http: HttpClient) {
    this.posUrl = 'http://localhost:8080/list-pos';
   }

   // get pos from the server
   public getPos(): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };
    return this.http.get<any>(this.posUrl, httpOptions);
   }

   // get pos by id
   public retrievePos(id: number): Observable<Pos> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*',
        'Content-Type':  'application/json'
      })
    };
    return this.http.get<Pos>(`${this.posUrl}/${id}`, httpOptions);
   }

   // add a pos and save it to the database
   public save(pos: Pos): Observable<Pos[]> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*',
        'Content-Type':  'application/json'
      })
    };
    return this.http.post<Pos[]>(this.posUrl, pos, httpOptions);
   }

   // alter the pos and save the changes
   update(id: number): Observable<Pos> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Access-control-Allow-origin': '*',
        'Content-Type': 'application/json'
      })
    };
    console.log(`${this.posUrl}/${id}`);
    return this.http.put<Pos>(`${this.posUrl}/${id}`, httpOptions);
   }

   // delete the pos from the server.
   delete(id: number): Observable<void> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*',
        'Content-Type':  'application/json'
      })
    };
    console.log(`${this.posUrl}/${id}`);
    return this.http.delete<void>(`${this.posUrl}/${id}`, httpOptions);
}

}
