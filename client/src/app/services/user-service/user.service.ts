import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  registerurl = 'http://localhost:8080/users/register';
  loginurl = 'http://localhost:8080/users/login';

  constructor(public httpClient: HttpClient) { }

  register(body: any): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };
    return this.httpClient.post(this.registerurl, body, httpOptions);
  }

  logIn(body: any): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*',
        'Content-Type':  'application/json'
      })
    };
    return this.httpClient.post(this.loginurl, body, httpOptions);
  }

}
