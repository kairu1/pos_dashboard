export class Pos {
    id: number;
    serialNumber: string;
    make: string;
    owner: string;
    status: string;
}
