import { Action } from '@ngrx/store';
import { Pos } from './pos';

export enum ActionTypes {
    ADD_POS = 'ADD_POS',
    DELETE_POS = 'DELETE_POS'
}

export class Add implements Action {
    readonly type = ActionTypes.ADD_POS;

    constructor(public payload: Pos) {}
}

export class Delete implements Action {
    readonly type = ActionTypes.DELETE_POS;

    constructor(public payload: number) {}
}

export type stateAction = Add | Delete;
