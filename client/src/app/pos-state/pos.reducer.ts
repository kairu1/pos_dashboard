import { Pos } from './pos';
import * as Methods from './pos.actions';

const initialState = [];

export function posReducer(state = initialState, action: Methods.stateAction) {
     switch (action.type) {
         case Methods.ActionTypes.ADD_POS:
         const pos: Pos = action.payload;
         return [...state, pos];

         case Methods.ActionTypes.DELETE_POS:
         return state.filter(data => {
           return data.id !== action.payload;
         });

         default:
         return state;
     }
 }
