import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {
  activate = false;
  redirectUrl: string;

  constructor(private router: Router) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
    const url: string = state.url;
    return this.checkLogin(url);
  }
  checkLogin(url: string): boolean {
    if (localStorage.getItem('isLoggedin')) {
      return true;
    }
    // store the attempted URL for redirecting
    this.redirectUrl = url;
    console.log(url);

    this.router.navigate(['/login']);
    return false;
  }


}
