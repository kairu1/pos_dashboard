import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { HomeComponent } from './components/home/home.component';
import { PosListComponent } from './components/pos-list/pos-list.component';
import { PosComponent } from './components/pos/pos.component';
import { AuthGuardService as AuthGuard } from './auth/auth-guard.service';
import { PosSalesComponent } from './components/pos-sales/pos-sales.component';

const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'home', component: HomeComponent,
  canActivate: [AuthGuard],
  children: [
    {path: '', redirectTo: 'pos', pathMatch: 'full'},
    {path: 'pos', component: PosListComponent, canActivateChild: [AuthGuard]},
    {path: 'addpos', component: PosComponent, canActivateChild: [AuthGuard]},
    {path: 'edit/:id', component: PosComponent, canActivateChild: [AuthGuard]},
    {path: 'list-pos-sales', component: PosSalesComponent, canActivateChild: [AuthGuard]},
  ]},
  {path: '', redirectTo: 'login', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
