import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { UserService } from '../../services/user-service/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm = this.fb.group({
    email: ['', [Validators.required, Validators.email]],
    username: ['', [Validators.required, Validators.minLength(4)]],
    password: ['', [Validators.required, Validators.minLength(8)]]
  });

  constructor(private fb: FormBuilder, private userService: UserService, private route: Router) { }

  ngOnInit() {
  }

  register() {
    this.userService.register(this.registerForm.value).subscribe(data => {
      console.log(data);
      if (data.valid) {
        this.route.navigate(['/login']);
      }
    });
  }

  onSubmit() {
    this.register();
    this.registerForm.reset();
  }

}
