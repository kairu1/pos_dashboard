import { Component, OnInit } from '@angular/core';
import { FormBuilder , Validators } from '@angular/forms';
import { UserService } from '../../services/user-service/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  validate: string;

  loginForm = this.fb.group({
    username: ['', [Validators.required, Validators.minLength(3)]],
    password: ['', [Validators.required, Validators.minLength(8)]]
});

  constructor(private fb: FormBuilder, private userService: UserService, private route: Router) { }

  ngOnInit() {
    localStorage.clear();
  }

  logIn() {
    this.userService.logIn(this.loginForm.value).subscribe(data => {
      console.log(data);
      this.validate = data.message;
      console.log('Verifying your credentials');
      if (data.valid) {
        localStorage.setItem('isLoggedin', 'true');
        this.route.navigate(['/home']);
      } else {console.log('You cannot access the dashboard'); }
    });
  }

  onSubmit() {
    this.logIn();
    this.loginForm.reset();
  }

}
