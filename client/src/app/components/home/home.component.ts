import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  title: string;
  reload;

  constructor(private route: Router) {

   }

  ngOnInit() {
    // this.reloadData();
    this.reload = this.listPos();
  }

  // reloadData() {
  //   this.reload = this.listPos();
  // }

  home() {
    this.route.navigate(['home']);
  }

  logOut() {
    this.route.navigate(['/login']);
  }

  listPos() {
    this.route.navigate(['home/pos']);
    // this.reloadData();
  }

  addPos() {
    this.route.navigate(['home/addpos']);
  }

  listPosSale() {
    this.route.navigate(['home/list-pos-sales']);
  }

}
