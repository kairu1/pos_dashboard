import { Component, OnInit, OnDestroy } from '@angular/core';
import { Pos } from '../../pos-state/pos';
import { Store } from '@ngrx/store';
import { Observable, Subject } from 'rxjs';
import * as Methods from '../../pos-state/pos.actions';

@Component({
  selector: 'app-pos-sales',
  templateUrl: './pos-sales.component.html',
  styleUrls: ['./pos-sales.component.css']
})
export class PosSalesComponent implements OnDestroy, OnInit {

  pos: Observable<Pos[]>;
  id = 0;

  dtTrigger: Subject<Pos[]> = new Subject();


  constructor(private store: Store<Pos[]>) {

   }

  ngOnInit(): void {

    return this.populate();

  }

  populate() {
    this.store.select('pos')
    .subscribe(data => {
      this.pos = data;
      this.dtTrigger.next();
    });
  }


  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }



  addPos(modelNumber: string, posAmount: number, posQuantity: number) {

    if (modelNumber === '' || posAmount === 0 || posQuantity === 0) {
      return;
    }

    this.store.dispatch(new Methods.Add({id: ++this.id, serialNumber: modelNumber, amount: posAmount, quantity: posQuantity}));
  }

  deletePos(id) {
    this.store.dispatch(new Methods.Delete(id));
    this.ngOnDestroy();
  }

}
