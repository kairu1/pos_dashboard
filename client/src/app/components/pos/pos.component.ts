import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PosService } from '../../services/pos-service/pos.service';
import { FormBuilder, Validators } from '@angular/forms';
import { Pos } from 'src/app/models/pos-model/pos';

@Component({
  selector: 'app-pos',
  templateUrl: './pos.component.html',
  styleUrls: ['./pos.component.css']
})
export class PosComponent implements OnInit {



  posForm = this.fb.group({
    serialNumber: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(8)]],
    make: ['', [Validators.required]],
    owner: ['', [Validators.required]],
    status: ['', [Validators.required]]
  });

  constructor(
    private route: Router,
    private router: ActivatedRoute,
    private fb: FormBuilder,
    private posService: PosService
  ) {}

  ngOnInit() {
    this.router.paramMap.subscribe(params => {
      const id = +params.get('id');
      if (id) {
        this.getPos(id);
      }
    });
  }

  getPos(id: number) {
    this.posService.retrievePos(id)
    .subscribe(
      (pos: Pos) =>
        this.editPos(pos),
        (err) => {
          console.log(err);
        }
    );
  }

  addItem() {
    console.log(this.posForm.value);
    this.posService.save(this.posForm.value)
    .subscribe(data => console.log(data));
  }

  editPos(pos: Pos) {
    this.posForm.patchValue({
      serialNumber: pos.serialNumber,
      make: pos.make,
      owner: pos.owner,
      status: pos.status
    });
  }

  submit(): void {
    this.addItem();
    this.posForm.reset();
    this.goToPosList();
  }

  goToPosList() {
    this.route.navigate(['/home/pos']);
  }

}
