import { Component, OnDestroy, OnInit } from '@angular/core';
import { PosService } from '../../services/pos-service/pos.service';
import { Pos } from 'src/app/models/pos-model/pos';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';


@Component({
  selector: 'app-pos-list',
  templateUrl: './pos-list.component.html',
  styleUrls: ['./pos-list.component.css']
})
export class PosListComponent implements OnDestroy, OnInit {

  pos;

  dtTrigger: Subject<Pos[]> = new Subject();

  constructor(
    private router: Router,
    private posService: PosService) { }

  ngOnInit() {
    this.reloadData();
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }

  reloadData() {
    this.pos = this.listPos();
  }

  listPos() {
    this.posService.getPos()
    .subscribe(data => {
      console.log(data);
      this.pos = data.Pos;
      this.dtTrigger.next();
    });
  }

  deletePos(id) {
    this.posService.delete(id)
    .subscribe(
      (data) => {
        console.log(data),
        this.reloadData();
        console.log(`Pos with ID = ${id} Deleted`);
    }, (err) => console.log(err));
  }

  editPos(id) {
    this.posService.update(id)
    .subscribe(
      (data) => {
        console.log(data);
      }
    );
    this.router.navigate(['/home/edit', id] );
  }

}
